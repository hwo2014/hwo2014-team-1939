import collections
from Bot.TrackPiece import TrackPiece

class Car(object):
    HISTORY_TICKS = 100
    PLANNED_SWITCH_COUNT = 10

    def __init__(self):
        """
        Constructor.
        """
        self.history = collections.deque([None] * Car.HISTORY_TICKS)
        self.planned_switches = collections.deque([TrackPiece.STRAIGHT] * Car.PLANNED_SWITCH_COUNT)

    def calculate_optimal_throttle(self, initial_velocity, target_velocity, distance):
        """
        Return the optimal throttle to accelerate from the initial_velocity to the
        target_velocity over distance.
        """
        pass
    
    def estimate_travel_distance(self, initial_velocity, throttle, time):
        """
        Return the estimated travel distance if traveling for time from 
        the initial_velocity with the specified throttle.
        """
        pass

    def estimate_velocity(self):
        """
        Return the current velocity estimated from history.
        """
        pass

    def save_history(self, position):
        """
        Save the Position object to the rotating history.
        """
        self.history.rotate(1)
        self.history[0] = position;

    @staticmethod
    def create_cars_from_json(cars_json):
        """
        Return a dictionary of Car objects constructed from JSON.
        """
        cars = dict()
        for car in cars_json:
            new_car = Car()
            new_car.name = car["id"]["name"]
            new_car.color = car["id"]["color"]
            new_car.length = car["dimensions"]["length"]
            new_car.width = car["dimensions"]["width"]
            new_car.flag_position = car["dimensions"]["guideFlagPosition"]
            cars[car["id"]["color"]] = new_car

        return cars