import math

class Position(object):

    def __init__(self):
        """
        Constructor.
        """
        pass

    def get_world_position(self):
        """
        Return the world position.
        """
        if self.in_piece.is_bend:
            return [self.in_piece.bend_center[0] + self.in_piece.radius * math.sin(self.in_piece_angle),
                    self.in_piece.bend_center[1] - self.in_piece.radius * math.cos(self.in_piece_angle)]
        else:
            delta_pos = [self.next_piece.start_pos[0] - self.in_piece.start_pos[0],
                         self.next_piece.start_pos[1] - self.in_piece.start_pos[1]]
            length = math.sqrt(delta_pos[0] * delta_pos[0] + delta_pos[1] * delta_pos[1])
            delta_pos = [delta_pos[0] / length, delta_pos[1] / length]
            return [self.in_piece.start_pos[0] + self.in_piece_distance * delta_pos[0],
                    self.in_piece.start_pos[1] + self.in_piece_distance * delta_pos[1]]

    @staticmethod
    def create_position_from_json(position_json, track_pieces):
        """
        Return a Position object created from JSON.
        """
        new_pos = Position()

        in_piece = position_json["piecePosition"]["pieceIndex"];
        next_piece = (in_piece + 1) % len(track_pieces)
        in_lane = position_json["piecePosition"]["lane"]["startLaneIndex"]
        target_lane = position_json["piecePosition"]["lane"]["endLaneIndex"]

        new_pos.in_piece = track_pieces[in_piece][in_lane]
        new_pos.next_piece = track_pieces[next_piece][target_lane]
        new_pos.in_piece_distance = position_json["piecePosition"]["inPieceDistance"]
        new_pos.angle = math.radians(position_json["angle"])
        new_pos.lap = position_json["piecePosition"]["lap"]

        new_pos.in_piece_angle = (new_pos.in_piece_distance / new_pos.in_piece.length) * new_pos.in_piece.angle
        if new_pos.in_piece.angle < 0:
            new_pos.in_piece_angle = (new_pos.in_piece.end_angle - new_pos.in_piece.angle) + new_pos.in_piece_angle + math.pi * 0.5
            new_pos.world_angle = new_pos.in_piece_angle + new_pos.angle - math.pi * 0.5
        else:
            new_pos.in_piece_angle = (new_pos.in_piece.end_angle - new_pos.in_piece.angle) + new_pos.in_piece_angle - math.pi * 0.5
            new_pos.world_angle = new_pos.in_piece_angle + new_pos.angle + math.pi * 0.5

        return new_pos