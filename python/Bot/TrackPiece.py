import math

class TrackPiece(object):
    SWITCH_LEFT, SWITCH_RIGHT, STRAIGHT = range(3)

    def __init__(self):
        """
        Constructor.
        """
        self.previous_pieces = dict()
        self.next_pieces = dict()

    def get_distance_to_piece(self, target_piece, in_distance = 0, target_in_distance = 0):
        """
        Return the distance from the in-distance of this piece to the in-distance in a target piece.
        """
        p_i = target_piece.piece_index
        l_i = target_piece.lane_index
        return self.piece.distance_to_piece[p_i][l_i] - in_distance + target_in_distance

    @staticmethod
    def create_track_from_json(track_json):
        """
        Return a 2D array of TrackPiece objects constructed from JSON.
        """
        piece_count = len(track_json["pieces"])
        lane_count = len(track_json["lanes"])
        track_pieces = [[None for p_i in range(lane_count)] for l_i in range(piece_count)]

        # Create pieces from json
        for p_i, piece in enumerate(track_json["pieces"]):
            for l_i, lane in enumerate(track_json["lanes"]):
                new_piece = TrackPiece()
                new_piece.piece_index = p_i
                new_piece.lane_index = l_i
                new_piece.is_switch = bool(piece.get("switch"))
                new_piece.is_bend = bool(piece.get("radius"))
                if new_piece.is_bend:
                    new_piece.angle = math.radians(piece.get("angle"))
                    new_piece.radius = piece["radius"] + lane["distanceFromCenter"] * (1 if new_piece.angle < 0 else -1)
                    new_piece.length = abs(new_piece.radius * math.pi * 2 * (new_piece.angle / (math.pi * 2)))
                else:
                    new_piece.angle = 0
                    new_piece.radius = 0
                    new_piece.length = piece["length"]
                track_pieces[p_i][l_i] = new_piece

        # Connect pieces
        for p_i in range(piece_count):
            for l_i, piece in enumerate(track_pieces[p_i]):
                previous_piece_index = (p_i - 1 + piece_count) % piece_count
                next_piece_index = (p_i + 1) % piece_count

                piece.previous_pieces[TrackPiece.STRAIGHT] = track_pieces[previous_piece_index][l_i]
                piece.next_pieces[TrackPiece.STRAIGHT] = track_pieces[next_piece_index][l_i]
                if piece.is_switch:
                    if l_i > 0:
                        piece.previous_pieces[TrackPiece.SWITCH_LEFT] = track_pieces[previous_piece_index][l_i - 1]
                        piece.next_pieces[TrackPiece.SWITCH_LEFT] = track_pieces[next_piece_index][l_i - 1]
                    elif l_i < lane_count - 1:
                        piece.previous_pieces[TrackPiece.SWITCH_RIGHT] = track_pieces[previous_piece_index][l_i + 1]
                        piece.next_pieces[TrackPiece.SWITCH_RIGHT] = track_pieces[next_piece_index][l_i + 1]
        
        # Create a distance lookup list between all track pieces
        for p_i in range(piece_count):
            for l_i, piece in enumerate(track_pieces[p_i]):
                piece.distance_to_piece = [[0 for l_i2 in range(lane_count)] for p_i2 in range(piece_count)]
                for p_i2 in range(piece_count):
                    for l_i2 in range(lane_count):
                        if not(p_i == p_i2 and l_i == l_i2):
                            scan_piece = piece
                            distance = 0
                            while not (scan_piece.piece_index == p_i2 and scan_piece.lane_index == l_i2):
                                distance += scan_piece.length
                                if scan_piece.lane_index > l_i2 and scan_piece.is_switch:
                                    scan_piece = scan_piece.next_pieces[TrackPiece.SWITCH_LEFT]
                                elif scan_piece.lane_index < l_i2 and scan_piece.is_switch:
                                    scan_piece = scan_piece.next_pieces[TrackPiece.SWITCH_RIGHT]
                                else:
                                    scan_piece = scan_piece.next_pieces[TrackPiece.STRAIGHT]     
                            piece.distance_to_piece[p_i2][l_i2] = distance
                            
        # Calculate absolute track positions in the world
        for p_i in range(piece_count):
            for piece in track_pieces[p_i]:
                prev = piece.previous_pieces[TrackPiece.STRAIGHT]

                if p_i == 0:
                    piece.end_angle = math.radians(track_json["startingPoint"]["angle"])
                    normal_angle = piece.end_angle + math.pi * 0.5
                    offset = [math.sin(normal_angle) * track_json["lanes"][piece.lane_index]["distanceFromCenter"],
                             -math.cos(normal_angle) * track_json["lanes"][piece.lane_index]["distanceFromCenter"]]
                    piece.start_pos = [track_json["startingPoint"]["position"]["x"] + offset[0],
                                       track_json["startingPoint"]["position"]["y"] + offset[1]]
                else:
                    piece.end_angle = prev.end_angle + piece.angle
                    piece.start_pos = prev.end_pos

                if piece.is_bend:
                    if piece.angle < 0:
                        piece.bend_center = [prev.end_pos[0] + piece.radius * math.sin(prev.end_angle - math.pi * 0.5),
                                             prev.end_pos[1] - piece.radius * math.cos(prev.end_angle - math.pi * 0.5)]
                        piece.end_pos = [piece.bend_center[0] + piece.radius * math.sin(piece.end_angle + math.pi * 0.5),
                                         piece.bend_center[1] - piece.radius * math.cos(piece.end_angle + math.pi * 0.5)]
                    else:
                        piece.bend_center = [prev.end_pos[0] + piece.radius * math.sin(prev.end_angle + math.pi * 0.5),
                                             prev.end_pos[1] - piece.radius * math.cos(prev.end_angle + math.pi * 0.5)]
                        piece.end_pos = [piece.bend_center[0] + piece.radius * math.sin(piece.end_angle - math.pi * 0.5),
                                         piece.bend_center[1] - piece.radius * math.cos(piece.end_angle - math.pi * 0.5)]
                else:
                    piece.end_pos = [piece.start_pos[0] + piece.length * math.sin(piece.end_angle),
                                     piece.start_pos[1] - piece.length * math.cos(piece.end_angle)]

        return track_pieces