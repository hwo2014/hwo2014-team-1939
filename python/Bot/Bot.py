import sys
from Network.Client        import Client
from Network.ClientCommand import ClientCommand
from Network.ClientReply   import ClientReply
from Bot.TrackPiece        import TrackPiece
from Bot.Position          import Position
from Bot.Car               import Car

class Bot(object):

    def __init__(self, host, port, name, key, primary_bot = None):
        """
        Constructor.
        """
        self.name = name
        self.key = key
        
        self.client = Client(host, port)
        if not self.client.connected:
            sys.exit()

        self.is_primary = primary_bot is None
        if self.is_primary:
            self.track_pieces = []
            self.cars = dict()
        else:
            self.track_pieces = primary_bot.track_pieces
            self.cars = primary_bot.cars
            
        self.bot_car = None
        self.game_started = False

        self.reply_handlers = {
            ClientReply.INVALID       : self.handle_INVALID,
            ClientReply.CREATERACE    : self.handle_CREATERACE,
            ClientReply.JOINRACE      : self.handle_JOINRACE,
            ClientReply.YOURCAR       : self.handle_YOURCAR,
            ClientReply.GAMEINIT      : self.handle_GAMEINIT,
            ClientReply.GAMESTART     : self.handle_GAMESTART,
            ClientReply.GAMEEND       : self.handle_GAMEEND,
            ClientReply.TOURNAMENTEND : self.handle_TOURNAMENTEND,
            ClientReply.CARPOSITIONS  : self.handle_CARPOSITIONS,
            ClientReply.CRASH         : self.handle_CRASH,
            ClientReply.SPAWN         : self.handle_SPAWN,
            ClientReply.DNF           : self.handle_DNF,
            ClientReply.LAPFINISHED   : self.handle_LAPFINISHED,
            ClientReply.FINISH        : self.handle_FINISH,
        }

    def run(self):
        """
        Run bot logic.
        """
        if not self.client.reply_queue.empty():
            reply = self.client.reply_queue.get()
            self.reply_handlers[reply.type](reply.data)

    def send_CREATERACE(self, car_count, track_name = None, password = None):
        print("Sending CREATERACE from " + self.name)
        data = { "botId": { "name": self.name,
                            "key" : self.key },
                 "carCount": car_count }
        if track_name is not None:
            data["trackName"] = track_name
        if password is not None:
            data["password"] = password
        self.client.command_queue.put(ClientCommand(ClientCommand.CREATERACE, data))

    def send_JOINRACE(self, car_count, track_name = None, password = None):
        print("Sending JOINRACE from " + self.name)
        data = { "botId": { "name": self.name,
                            "key" : self.key },
                 "carCount": car_count }
        if track_name is not None:
            data["trackName"] = track_name
        if password is not None:
            data["password"] = password
        self.client.command_queue.put(ClientCommand(ClientCommand.JOINRACE, data))

    def send_PING(self):
        #print("Sending PING from " + self.name)
        self.client.command_queue.put(ClientCommand(ClientCommand.PING))

    def send_THROTTLE(self, throttle):
        #print("Sending THROTTLE from " + self.name)
        self.client.command_queue.put(ClientCommand(ClientCommand.THROTTLE, throttle))

    def send_SWITCHLANE(self, switch_type):
        print("Sending SWITCHLANE from " + self.name)
        switch_name = "Left" if switch_type == TrackPiece.SWITCH_LEFT else "Right"
        self.client.command_queue.put(ClientCommand(ClientCommand.SWITCHLANE, switch_name))

    def handle_INVALID(self, data):
        print("Invalid reply to " + self.name)
        self.send_PING()

    def handle_CREATERACE(self, data):
        print("Received CREATERACE for " + self.name)
        self.send_PING()

    def handle_JOINRACE(self, data):
        print("Received JOINRACE for " + self.name)
        self.send_PING()

    def handle_YOURCAR(self, data):
        print("Received YOURCAR for " + self.name)
        self.send_PING()

    def handle_GAMEINIT(self, data):
        print("Received GAMEINIT for " + self.name)
        if self.is_primary:
            self.track_pieces = TrackPiece.create_track_from_json(data["race"]["track"])
            self.cars = Car.create_cars_from_json(data["race"]["cars"])
        for car in self.cars.values():
            if car.name == self.name:
                self.bot_car = car
        self.send_PING()

    def handle_GAMESTART(self, data):
        print("Received GAMESTART for " + self.name)
        self.game_started = True
        self.send_PING()

    def handle_GAMEEND(self, data):
        print("Received GAMEEND for " + self.name)
        self.game_started = False
        self.send_PING()

    def handle_TOURNAMENTEND(self, data):
        print("Received TOURNAMENTEND for " + self.name)
        self.send_PING()

    def handle_CARPOSITIONS(self, data):
        #print("Received CARPOSITIONS for " + self.name)
        if self.is_primary:
            for car in data:
                new_pos = Position.create_position_from_json(car, self.track_pieces)
                self.cars[car["id"]["color"]].save_history(new_pos)
        
        if self.game_started:
            self.send_THROTTLE(1)
        else:
            self.send_PING()

    def handle_CRASH(self, data):
        print("Received CRASH for " + self.name)
        self.send_PING()

    def handle_SPAWN(self, data):
        print("Received SPAWN for " + self.name)
        self.send_PING()

    def handle_DNF(self, data):
        print("Received DNF for " + self.name)
        self.send_PING()

    def handle_LAPFINISHED(self, data):
        print("Received LAPFINISHED for " + self.name)
        self.send_PING()

    def handle_FINISH(self, data):
        print("Received FINISH for " + self.name)
        self.send_PING()