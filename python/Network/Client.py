import socket
import queue
import threading
import json
from Network.ClientReply import ClientReply

class Client(object):

    def __init__(self, host, port):
        """
        Constructor.
        """
        self.command_queue = queue.Queue()
        self.reply_queue = queue.Queue()

        self.connected = False
        try:
            print("Connecting to " + host + ":" + str(port))
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.connect((host, int(port)))
            self.connected = True
        except IOError as e:
            print("Unable to connect: " + str(e))
            return

        thread = threading.Thread(target = self.run)
        thread.daemon = True
        thread.start()

    def close(self):
        """
        Close the socket.
        """
        self.socket.close()

    def run(self):
        """
        Socket read and write thread.
        """
        socket_file = self.socket.makefile()
        while True:
            command = self.command_queue.get(True)
            self.socket.send(str(command.data + "\n").encode(encoding="utf_8"))

            line = socket_file.readline()
            try:
                json_object = json.loads(line)
                self.reply_queue.put(ClientReply(json_object['msgType'], json_object['data']))
            except ValueError:
                self.reply_queue.put(ClientReply("invalid"))