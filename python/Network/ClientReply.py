class ClientReply(object):
    INVALID, CREATERACE, JOINRACE = range(3)
    YOURCAR, GAMEINIT, GAMESTART, GAMEEND, TOURNAMENTEND = range(3, 8)
    CARPOSITIONS, CRASH, SPAWN, DNF, LAPFINISHED, FINISH = range(8, 14)

    string_to_type = {
        "createRace"    : CREATERACE,
        "joinRace"      : JOINRACE,
        "yourCar"       : YOURCAR,
        "gameInit"      : GAMEINIT,
        "gameStart"     : GAMESTART,
        "gameEnd"       : GAMEEND,
        "tournamentEnd" : TOURNAMENTEND,
        "carPositions"  : CARPOSITIONS,
        "crash"         : CRASH,
        "spawn"         : SPAWN,
        "dnf"           : DNF,
        "lapFinished"   : LAPFINISHED,
        "finish"        : FINISH,
    }

    def __init__(self, reply_type, data = {}):
        """
        Constructor.
        """
        self.type_string = reply_type
        if reply_type in ClientReply.string_to_type:
            self.type = ClientReply.string_to_type[reply_type]
        else:
            self.type = ClientReply.INVALID
        self.data = data