import json

class ClientCommand(object):
    PING, THROTTLE, SWITCHLANE, CREATERACE, JOINRACE = range(5)

    type_to_string = {
        PING       : "ping",
        THROTTLE   : "throttle",
        SWITCHLANE : "switchLane",
        CREATERACE : "createRace",
        JOINRACE   : "joinRace"
    }

    def __init__(self, command_type, data = {}):
        """
        Constructor.
        """
        self.type = command_type
        self.data = json.dumps({"msgType": ClientCommand.type_to_string[command_type], "data": data})