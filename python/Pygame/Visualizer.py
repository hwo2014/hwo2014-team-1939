import pygame
import math
import sys

class Visualizer(object):
    RESOLUTION = (1280, 720)
    TRACK_SCREEN_PADDING = 64

    def __init__(self):
        """
        Constructor.
        """
        pygame.init()
        self.screen = pygame.display.set_mode(Visualizer.RESOLUTION, 0, 32)
        
        self.position = [0, 0]
        self.scale = 1
    
    def draw(self, cars, track_pieces):
        """
        Draw cars circles and vectors.
        Draw track arcs and lines.
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
        
        self.screen.fill([0, 0, 0])
        
        for car in cars.values():
            if car.history[0] is None: continue
            position = car.history[0]
            screen_pos = self.point_to_screen(position.get_world_position())
            forward = [screen_pos[0] + math.sin(position.world_angle) * 32,
                       screen_pos[1] - math.cos(position.world_angle) * 32]
            pygame.draw.circle(self.screen, [128, 128, 255], screen_pos, 8, 2)
            pygame.draw.line(self.screen, [0, 255, 0], screen_pos, forward, 2)
            
        for p_i in range(len(track_pieces)):
            for piece in track_pieces[p_i]:
                if piece.is_bend:
                    bend_rect = [piece.bend_center[0] - piece.radius,
                                 piece.bend_center[1] - piece.radius,
                                 piece.radius * 2, piece.radius * 2]
                    if piece.angle < 0:
                        start_angle = - piece.end_angle + piece.angle
                        end_angle = - piece.end_angle
                    else:
                        start_angle = math.pi - piece.end_angle
                        end_angle = math.pi - piece.end_angle + piece.angle
                    pygame.draw.arc(self.screen, [255, 0, 0], self.rect_to_screen(bend_rect), start_angle, end_angle, 1)
                else:
                    for next_piece in piece.next_pieces.values():
                        pygame.draw.line(self.screen, [255, 0, 0],
                                         self.point_to_screen(piece.start_pos),
                                         self.point_to_screen(next_piece.start_pos), 1)
        
        pygame.display.update()

    def point_to_screen(self, point):
        """
        Return a point converted from world to screen space.
        """
        return [int(point[0] * self.scale - self.position[0]),
                int(point[1] * self.scale - self.position[1])]

    def rect_to_screen(self, rect):
        """
        Return a rectangle converted from world to screen space.
        """
        return [int(rect[0] * self.scale - self.position[0]),
                int(rect[1] * self.scale - self.position[1]),
                rect[2] * self.scale, rect[3] * self.scale]

    def fit_to_track(self, track_pieces):
        """
        Adjust the viewport position and scale to fit all the track pieces.
        """
        upper_left_corner = [100000, 100000]
        lower_right_corner = [-100000, -100000]
        for p_i in range(len(track_pieces)):
            for piece in track_pieces[p_i]:
                upper_left_corner = [min(piece.start_pos[0], piece.end_pos[0], upper_left_corner[0]),
                                     min(piece.start_pos[1], piece.end_pos[1], upper_left_corner[1])]
                lower_right_corner = [max(piece.start_pos[0], piece.end_pos[0], lower_right_corner[0]),
                                      max(piece.start_pos[1], piece.end_pos[1], lower_right_corner[1])]
        upper_left_corner = [upper_left_corner[0] - Visualizer.TRACK_SCREEN_PADDING,
                             upper_left_corner[1] - Visualizer.TRACK_SCREEN_PADDING]
        lower_right_corner = [lower_right_corner[0] + Visualizer.TRACK_SCREEN_PADDING,
                              lower_right_corner[1] + Visualizer.TRACK_SCREEN_PADDING]
        min_scale = [Visualizer.RESOLUTION[0] / (lower_right_corner[0] - upper_left_corner[0]),
                     Visualizer.RESOLUTION[1] / (lower_right_corner[1] - upper_left_corner[1])]
        self.scale = min_scale[0] if min_scale[0] < min_scale[1] else min_scale[1]
        self.position = [upper_left_corner[0] * self.scale, upper_left_corner[1] * self.scale]