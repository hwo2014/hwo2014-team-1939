import sys
import time
from Bot.Bot import Bot
try:
    from Pygame.Visualizer import Visualizer
    use_visualizer = True
except ImportError:
    use_visualizer = False

class BotManager(object):
    LOOP_SLEEP_SECONDS = 1 / 200

    def __init__(self, host, port, key):
        """
        Constructor.
        """
        if use_visualizer:
            self.visualizer = None
        
        self.primary_bot = Bot(host, port, "War", key)
        self.bots = [
            self.primary_bot,
            Bot(host, port, "Famine", key, self.primary_bot),
            Bot(host, port, "Pestilence", key, self.primary_bot),
            Bot(host, port, "Death", key, self.primary_bot),
        ]
        
        self.bots[0].send_CREATERACE(4, "keimola", "test_server_#_10000")
        time.sleep(1 / 4)
        self.bots[1].send_JOINRACE(4, "keimola", "test_server_#_10000")
        time.sleep(1 / 4)
        self.bots[2].send_JOINRACE(4, "keimola", "test_server_#_10000")
        time.sleep(1 / 4)
        self.bots[3].send_JOINRACE(4, "keimola", "test_server_#_10000")
        
    def run(self):
        """
        Main loop.
        """
        while True:
            for bot in self.bots:
                bot.run()

            if use_visualizer:
                if self.visualizer is None and len(self.primary_bot.track_pieces) > 0:
                    self.visualizer = Visualizer()
                    self.visualizer.fit_to_track(self.primary_bot.track_pieces)
                elif self.visualizer is not None:
                    self.visualizer.draw(self.primary_bot.cars, self.primary_bot.track_pieces)
            
            time.sleep(BotManager.LOOP_SLEEP_SECONDS)

if __name__ == "__main__":
    host, port, key = sys.argv[1:4]
    bot = BotManager(host, port, key)
    bot.run()